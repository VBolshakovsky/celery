from os import environ
from dotenv import load_dotenv

load_dotenv()


class Configcelery_beat:
    #Общие
    APP_NAME = environ.get('APP_NAME', 'celery_beat')
    TIME_ZONE = environ.get('TIME_ZONE', 'Europe/Moscow')

    #REDIS
    REDIS_HOST = environ.get('REDIS_HOST', 'localhost')
    REDIS_PORT = environ.get('REDIS_PORT', '6379')
    REDIS_PASSWORD = environ.get('REDIS_PASSWORD', '')
    REDIS_DB = environ.get('REDIS_DB', 0)

    #ubs_event schedule
    TASK_EVENT_HOURS = environ.get('TASK_EVENT_HOURS', 0)
    TASK_EVENT_MINUTES = environ.get('TASK_EVENT_MINUTES', 0)
    TASK_EVENT_SECONDS = environ.get('TASK_EVENT_SECONDS', 0)