"""
Description: Celery beat tasks
Python version: 3.11.2
Author: V.Bolshakov
Creation date: 27.06.2023
Links:
    # Описание консольных комманд
    #https://docs.celeryq.dev/en/latest/userguide/monitoring.html#monitoring-control
    # Настройки
    #https://docs.celeryq.dev/en/latest/userguide/configsuration.html
    # Переодические задачи
    #https://docs.celeryq.dev/en/latest/userguide/periodic-tasks.html
"""

import celery
from datetime import datetime, timedelta
from src.config import Configcelery_beat as settings

# redis://:password@hostname:port/db_number
# https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/redis.html
broker_redis = f'redis://:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:{settings.REDIS_PORT}/{settings.REDIS_DB}'

#amqp://myuser:mypassword@localhost:5672/myvhost
#https://docs.celeryq.dev/en/stable/getting-started/backends-and-brokers/rabbitmq.html
#broker_rebbit = f'amqp://{settings.RMQ_USER}:{settings.RMQ_PASSWORD}@{settings.RMQ_HOST}:{settings.RMQ_PORT}//'

app_celery = celery.Celery('tasks', broker=broker_redis, backend=broker_redis)
app_celery.conf.timezone = settings.TIME_ZONE
app_celery.autodiscover_tasks()
#app_celery.autodiscover_tasks(force=True)


@app_celery.task
def task_example():
    """
    Функция для запуска периодической задачи celery
    """
    try:
        print("|-------------Hello world-------------|")
    except Exception as err:
        print(f"ОШИБКА! выполнения task: {err}")
        raise


app_celery.conf.beat_schedule = {
    'task_example': {
        'task':
        'src.tasks.tasks.task_example',
        'schedule':
        timedelta(hours=int(settings.TASK_EVENT_HOURS),
                  minutes=int(settings.TASK_EVENT_MINUTES),
                  seconds=int(settings.TASK_EVENT_SECONDS))
    }
}

# Отладка
task_example()

#celery -A src.tasks.tasks:app_celery worker -l INFO -n celery_beat --pool=solo
#celery -A src.tasks.tasks:app_celery beat -l INFO -s C:\Users\.......\Documents\Project\celery\celery_beat\log\celery\schedule\schedule.db
#celery -A src.tasks.tasks:app_celery flower -l INFO

#celery -A src.tasks.tasks:app_celery worker -l ERROR -n ubs_gate --pool=solo
#celery -A src.tasks.tasks:app_celery beat -l ERROR -s C:\Users\......\Documents\Project\ubs_gate_fastapi\ubs_gate\log\celery\schedule\schedule.db
#celery -A src.tasks.tasks:app_celery flower -l ERROR

#В DOCKER проверить запуск скрипта
#docker build -t celery_beat:latest .\celery_beat\
#docker run -t -i celery_beat /bin/sh
#python3 ./src/tasks/tasks.py

#http://localhost:5555/
