# celery_beat

Celery beat example

## Установка

#celery -A src.tasks.tasks:app_celery worker -l INFO -n celery_beat --pool=solo
#celery -A src.tasks.tasks:app_celery beat -l INFO -s C:\Users\.......\Documents\Project\celery\celery_beat\log\celery\schedule\schedule.db
#celery -A src.tasks.tasks:app_celery flower -l INFO

docker build --pull --rm -f "celery_beat\dockerfile" -t ubsgate:latest "celery_beat"
docker run --dns=8.8.8.8 -t -i celery_beat /bin/sh
docker compose -f "celery_beat\docker-compose.yml" up -d --build
docker compose -f "celery_beat\docker-compose.yml" down

http://localhost:5555/

## Использование


## Лицензия

No license file
