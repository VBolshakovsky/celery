FROM python:slim-buster

RUN apt-get update \
    && apt-get upgrade -y

# Установка Компонентов системы для отладки(не обязательно)
RUN apt-get install nano \
    iputils-ping \
    net-tools \
    dnsutils \
    mc \
    -y

RUN mkdir /app

COPY /requirements /app/requirements

WORKDIR /app

RUN pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --upgrade pip \
    && pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org --upgrade setuptools \
    && pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org -r ./requirements/base.txt

COPY . .

RUN pip install --trusted-host pypi.org --trusted-host pypi.python.org --trusted-host files.pythonhosted.org -e .