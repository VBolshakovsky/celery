from setuptools import find_packages, setup

setup(
    name='celery_beat',
    packages=find_packages(),
    version='0.1.0',
    description='Celery beat example',
    author='V.Bolshakov',
)