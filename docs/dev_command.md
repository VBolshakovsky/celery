# Requirements
python -m venv venv
venv\Scripts\Activate.ps1
. bin/activate

python -m pip install --upgrade pip

pip install -r .\celery_beat\requirements\base.txt

pip install  -e .\celery_beat

pip freeze > .\celery_beat\requirements\prod.txt

pip uninstall -r -y .\celery_beat\requirements\base.txt
pip uninstall -y celery_beat

# Docker

docker build --pull --rm -f "celery_beat\dockerfile" -t ubsgate:latest "celery_beat"

docker run --dns=8.8.8.8 -t -i celery_beat /bin/sh

docker compose -f "celery_beat\docker-compose.yml" up -d --build

docker compose -f "celery_beat\docker-compose.yml" down

# FASTAPI
cd celery_beat\src

uvicorn main:app_api --reload
uvicorn main:app_api --reload --port 8000 --host 0.0.0.0 --workers 4

http://127.0.0.1:8000
http://127.0.0.1:8000/docs
http://127.0.0.1:8000/redoc