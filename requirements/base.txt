# local package(setup.py)
# -e .\\celery_beat
yapf
python-dotenv
redis
celery==5.3.0
flower==1.2.0